#!/bin/bash

# 
# Create Jenkins create job
# 

cat <<EOF | java -jar /opt/jenkins-cli.jar -s http://localhost:8080/ create-job Docker-GitHub-Final --username admin --password admin
<?xml version='1.1' encoding='UTF-8'?>
<project>
  <actions/>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties>
    <com.coravy.hudson.plugins.github.GithubProjectProperty plugin="github@1.29.3">
      <projectUrl>https://github.com/shekeriev/dob-2018-sep-exam-1.git/</projectUrl>
      <displayName></displayName>
    </com.coravy.hudson.plugins.github.GithubProjectProperty>
  </properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@3.9.1">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <url>https://github.com/shekeriev/dob-2018-sep-exam-1.git</url>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/master</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"/>
    <extensions/>
  </scm>
  <assignedNode>docker</assignedNode>
  <canRoam>false</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
  <triggers>
    <hudson.triggers.SCMTrigger>
      <spec>H/2 * * * *</spec>
      <ignorePostCommitHooks>false</ignorePostCommitHooks>
    </hudson.triggers.SCMTrigger>
  </triggers>
  <concurrentBuild>false</concurrentBuild>
  <customWorkspace>/vagrant/www-dynamic</customWorkspace>
  <builders>
    <hudson.tasks.Shell>
      <command>cd /vagrant/www-dynamic/php
docker image build -t dob-php .
docker tag dob-php:latest dobnasko/dob-php:latest
docker push dobnasko/dob-php:latest</command>
    </hudson.tasks.Shell>
    <hudson.tasks.Shell>
      <command>cd /vagrant/www-dynamic/mysql
docker image build -t dob-mysql .
docker tag dob-mysql:latest dobnasko/dob-mysql:latest
docker push dobnasko/dob-mysql:latest</command>
    </hudson.tasks.Shell>
    <hudson.tasks.Shell>
      <command>cd /vagrant/www-dynamic/nginx
docker image build -t dob-nginx .
docker tag dob-nginx:latest dobnasko/dob-nginx:latest
docker push dobnasko/dob-nginx:latest</command>
    </hudson.tasks.Shell>
    <hudson.tasks.Shell>
      <command>cd /vagrant/docker/
docker stack rm dob-stack || true
sleep 5
docker stack deploy -c dob-stack.yml dob-stack
</command>
    </hudson.tasks.Shell>
  </builders>
  <publishers/>
  <buildWrappers/>
</project>
EOF
