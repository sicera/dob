#!/bin/bash

echo "* Install Ansible ..."
sudo yum install -y ansible python-passlib

echo "* Set Ansible configuration in /etc/ansible/ansible.cfg ..."
cp /vagrant/ansible/ansible.cfg /etc/ansible/ansible.cfg

echo "* Set Ansible global inventory in /etc/ansible/hosts ..."
cp /vagrant/ansible/hosts /etc/ansible/hosts

echo "* Link Ansible playbooks in /playbooks/ ..."
ln -s /vagrant/ansible/playbooks /playbooks

echo "* Prepare /playbooks/roles folder ..."
ln -s /vagrant/ansible/roles /playbooks/roles

echo "* Install external Ansible role(s) in /playbooks/roles/ ..."
ansible-galaxy install geerlingguy.jenkins -p /playbooks/roles/
ansible-galaxy install geerlingguy.pip -p /playbooks/roles/
ansible-galaxy install Bessonov.docker-swarm -p /playbooks/roles/
ansible-galaxy install geerlingguy.docker -p /playbooks/roles/

echo "* Execute Ansible Playbooks ..."
ansible-playbook /playbooks/install-all.yml

