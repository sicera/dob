# -*- mode: ruby -*-
# vi: set ft=ruby :
# Copyright 2013 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Usage:
#  1) Launch both instances and then destroy both
#     $ vagrant up --provider=google
#     $ vagrant destroy
#  2) Launch one instance 'z1c' and then destory the same
#     $ vagrant up z1c --provider=google
#     $ vagrant destroy z1c

# Customize these global variables
$GOOGLE_PROJECT_ID = "dob-exam"
$GOOGLE_CLIENT_EMAIL = "105633586943-compute@developer.gserviceaccount.com"
$GOOGLE_JSON_KEY_LOCATION = "/home/nasko/Projects/dob/dob-exam-ea0a92becf29.json"
$LOCAL_USER = "vagrant"
$LOCAL_SSH_KEY = "../dob-exam.priv"

# Example provision script
$PROVISION_OS = <<SCRIPT
uname=$(uname -a)
ver=$(cat /etc/redhat-release)
echo "== BEGIN: vagrant provisioning on '${uname}'"
echo "== OS VERSION: ${ver}"
echo "== UPDATING repositories and packages and adding bash-completion"
yum update -y
yum install -y bash-completion
extip=$(curl -s http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip -H "Metadata-Flavor: Google")
echo "== EXTERNAL IP: ${extip}"
echo "== APPENDING /etc/motd"
d=$(date +%r)
echo "# ${d}" >> /etc/motd
echo "== cat /etc/motd"
cat /etc/motd
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.box = "google/gce"
  config.vm.provision :shell, :inline => $PROVISION_OS
  config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/"

  config.vm.define :ahost do |z1c|
    z1c.vm.provider :google do |google, override|
      google.google_project_id = $GOOGLE_PROJECT_ID
      google.google_client_email = $GOOGLE_CLIENT_EMAIL
      google.google_json_key_location = $GOOGLE_JSON_KEY_LOCATION
      google.zone = "us-east1-c"

      override.ssh.username = $LOCAL_USER
      override.ssh.private_key_path = $LOCAL_SSH_KEY

      google.zone_config "us-east1-c" do |z1c_zone|
        z1c_zone.name = "ahost"
        z1c_zone.network = "sulab-exam"
        z1c_zone.subnetwork = "sulab-exam-us-e1"
        z1c_zone.image_family = 'centos-7'
        z1c_zone.machine_type = "f1-micro"
        z1c_zone.zone = "us-east1-c"
        z1c_zone.metadata = {"zone" => "US East 1c"}
#        z1c_zone.tags = [ 'http-server', 'https-server' ]
      end
    end
    z1c.vm.provision "shell", path: "ansible.sh"
  end

  config.vm.define :nhost do |z1c|
    z1c.vm.provider :google do |google, override|
      google.google_project_id = $GOOGLE_PROJECT_ID
      google.google_client_email = $GOOGLE_CLIENT_EMAIL
      google.google_json_key_location = $GOOGLE_JSON_KEY_LOCATION
      google.zone = "us-east1-c"

      override.ssh.username = $LOCAL_USER
      override.ssh.private_key_path = $LOCAL_SSH_KEY

      google.zone_config "us-east1-c" do |z1c_zone|
        z1c_zone.name = "nhost"
        z1c_zone.network = "sulab-exam"
        z1c_zone.subnetwork = "sulab-exam-us-e1"
        z1c_zone.image_family = 'centos-7'
        z1c_zone.machine_type = "f1-micro"
        z1c_zone.zone = "us-east1-c"
        z1c_zone.metadata = {"zone" => "US East 1c"}
        z1c_zone.tags = [ 'http-server' ]
      end
    end
    z1c.vm.provision "shell", inline: 'curl -4 "https://dob-nagios.sicera.org:dob-exam-key@dyn.dns.he.net/nic/update?hostname=dob-nagios.sicera.org"', run: "always"
  end

  config.vm.define :jhost do |z1f|
    z1f.vm.provider :google do |google, override|
      google.google_project_id = $GOOGLE_PROJECT_ID
      google.google_client_email = $GOOGLE_CLIENT_EMAIL
      google.google_json_key_location = $GOOGLE_JSON_KEY_LOCATION
      google.zone = "us-east1-b"

      override.ssh.username = $LOCAL_USER
      override.ssh.private_key_path = $LOCAL_SSH_KEY

      google.zone_config "us-east1-b" do |z1f_zone|
        z1f_zone.name = "jhost"
        z1f_zone.network = "sulab-exam"
        z1f_zone.subnetwork = "sulab-exam-us-e1"
        z1f_zone.image_family = 'centos-7'
        z1f_zone.machine_type = "g1-small"
        z1f_zone.zone = "us-east1-b"
        z1f_zone.metadata = {"zone" => "US East 1b"}
        z1f_zone.tags = [ 'http-8080-server' ]
      end
    end
    z1f.vm.provision "shell", inline: 'curl -4 "https://dob-jenkins.sicera.org:dob-exam-key@dyn.dns.he.net/nic/update?hostname=dob-jenkins.sicera.org"', run: "always"
  end

  config.vm.define :d1host do |z1f|
    z1f.vm.provider :google do |google, override|
      google.google_project_id = $GOOGLE_PROJECT_ID
      google.google_client_email = $GOOGLE_CLIENT_EMAIL
      google.google_json_key_location = $GOOGLE_JSON_KEY_LOCATION
      google.zone = "us-east1-b"

      override.ssh.username = $LOCAL_USER
      override.ssh.private_key_path = $LOCAL_SSH_KEY

      google.zone_config "us-east1-b" do |z1f_zone|
        z1f_zone.name = "d1host"
        z1f_zone.network = "sulab-exam"
        z1f_zone.subnetwork = "sulab-exam-us-e1"
        z1f_zone.can_ip_forward = true
        z1f_zone.image_family = 'centos-7'
        z1f_zone.machine_type = "g1-small"
        z1f_zone.zone = "us-east1-b"
        z1f_zone.metadata = {"zone" => "US East 1b"}
        z1f_zone.tags = [ 'http-server', 'https-server' ]
      end
    end
    z1f.vm.provision "shell", inline: 'curl -4 "https://dob-docker.sicera.org:dob-exam-key@dyn.dns.he.net/nic/update?hostname=dob-docker.sicera.org"', run: "always"
  end

  config.vm.define :d2host do |z1f|
    z1f.vm.provider :google do |google, override|
      google.google_project_id = $GOOGLE_PROJECT_ID
      google.google_client_email = $GOOGLE_CLIENT_EMAIL
      google.google_json_key_location = $GOOGLE_JSON_KEY_LOCATION
      google.zone = "us-east1-d"

      override.ssh.username = $LOCAL_USER
      override.ssh.private_key_path = $LOCAL_SSH_KEY

      google.zone_config "us-east1-d" do |z1f_zone|
        z1f_zone.name = "d2host"
        z1f_zone.network = "sulab-exam"
        z1f_zone.subnetwork = "sulab-exam-us-e1"
        z1f_zone.can_ip_forward = true
        z1f_zone.image_family = 'centos-7'
        z1f_zone.machine_type = "g1-small"
        z1f_zone.zone = "us-east1-d"
        z1f_zone.metadata = {"zone" => "US East 1d"}
        z1f_zone.tags = [ 'http-server', 'https-server' ]
      end
    end
  end

  config.vm.define :d3host do |z1f|
    z1f.vm.provider :google do |google, override|
      google.google_project_id = $GOOGLE_PROJECT_ID
      google.google_client_email = $GOOGLE_CLIENT_EMAIL
      google.google_json_key_location = $GOOGLE_JSON_KEY_LOCATION
      google.zone = "us-east1-c"

      override.ssh.username = $LOCAL_USER
      override.ssh.private_key_path = $LOCAL_SSH_KEY

      google.zone_config "us-east1-c" do |z1f_zone|
        z1f_zone.name = "d3host"
        z1f_zone.network = "sulab-exam"
        z1f_zone.subnetwork = "sulab-exam-us-e1"
        z1f_zone.can_ip_forward = true
        z1f_zone.image_family = 'centos-7'
        z1f_zone.machine_type = "g1-small"
        z1f_zone.zone = "us-east1-c"
        z1f_zone.metadata = {"zone" => "US East 1c"}
        z1f_zone.tags = [ 'http-server', 'https-server' ]
      end
    end
  end
end
