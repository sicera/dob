#!/bin/bash

# Author: Erik Kristensen
# Email: erik@erikkristensen.com
# License: MIT
# Nagios Usage: check_nrpe!check_docker_container!_container_id_
# Usage: ./check_docker_container.sh _container_id_
#
# Depending on your docker configuration, root might be required. If your nrpe user has rights
# to talk to the docker daemon, then root is not required. This is why root privileges are not
# checked.
#
# The script checks if a container is running.
#   OK - running
#   WARNING - restarting
#   CRITICAL - stopped
#   UNKNOWN - does not exist
#
# CHANGELOG - March 20, 2017
#  - Removes Ghost State Check, Checks for Restarting State, Properly finds the Networking IP addresses
#  - Returns unknown (exit code 3) if docker binary is missing, unable to talk to the daemon, or if container id is missing

CONTAINER=$1

if [ "x${CONTAINER}" == "x" ]; then
  echo "UNKNOWN - Container ID or Friendly Name Required"
  exit 3
fi

if [ "x$(which docker)" == "x" ]; then
  echo "UNKNOWN - Missing docker binary"
  exit 3
fi

docker info > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "UNKNOWN - Unable to talk to the docker daemon"
  exit 3
fi

RUNNING=$(docker service ps $CONTAINER 2> /dev/null)

if [ $? -ne 0 ]; then
  echo "CRITICAL - Service not healthy!"
  exit 3
fi

STARTED=$(docker service ps $CONTAINER)

echo "OK - $CONTAINER is running. Started: $STARTED"
